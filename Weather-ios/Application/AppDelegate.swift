//
//  AppDelegate.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		window = UIWindow.init(frame: UIScreen.main.bounds)
		window?.backgroundColor = UIColor.white
		AppManager.shared.updateRootViewController()
		window?.makeKeyAndVisible()
		
		return true
	}
}

