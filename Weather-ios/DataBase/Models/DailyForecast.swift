//
//  DailyForecast.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit
import RealmSwift

class DailyForecast: BaseModel {
	
	@objc dynamic var iconPath: String?
	@objc dynamic var minTemperature: Double = 0.0
	@objc dynamic var maxTemperature: Double = 0.0
	@objc dynamic var precipitation: Int = 0
	@objc dynamic var windSpeed: Double = 0.0
	
	
	override static func primaryKey() -> String? {
		return "id"
	}

	var day: String {
		return Date(timeIntervalSince1970: TimeInterval(exactly: id) ?? 0).baseConvertToString()
	}
	
	static func parseJson(_ json: [String: AnyObject]) -> DailyForecast {
		let forecast = DailyForecast()
		forecast.id = json["EpochDate"] as? Int ?? 0
		
		if let temperature = json["Temperature"] as? [String: AnyObject] {
			if let data = temperature["Minimum"] as? [String: AnyObject] {
				forecast.minTemperature = data["Value"] as? Double ?? 0.0
			}
			
			if let data = temperature["Maximum"] as? [String: AnyObject] {
				forecast.maxTemperature = data["Value"] as? Double ?? 0.0
			}
		}
		
		if let day = json["Day"] as? [String: AnyObject] {
			if let icon = day["Icon"] as? Int {
				let iconStr = icon < 10 ? "0\(icon)" : "\(icon)"
				forecast.iconPath = "https://developer.accuweather.com/sites/default/files/\(iconStr)-s.png"
			}
			
			forecast.precipitation = day["PrecipitationProbability"] as? Int ?? 0
			
			if let windData = day["Wind"] as? [String: AnyObject],
				let speed = windData["Speed"] as? [String: AnyObject] {
				forecast.windSpeed = speed["Value"] as? Double ?? 0.0
			}
		}
		
		return forecast
	}
}
