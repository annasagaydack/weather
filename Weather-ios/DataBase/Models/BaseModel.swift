//
//  BaseModel.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import Foundation
import RealmSwift

class BaseModel: Object {

	@objc dynamic var id: Int = 0
}

protocol BaseModelHelper {}

extension BaseModel: BaseModelHelper {}

extension BaseModelHelper where Self: BaseModel {
	
	static func allObjects() -> [Self] {
		let realm = try! Realm()
		return realm.objects(Self.self).array()
	}
}
