//
//  CurrentConditions.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/23/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

class CurrentConditions {

	var iconPath: String?
	var temperature: Double = 0.0
	var weatherText: String?
	
	static func parseJson(_ json: [String: AnyObject]) -> CurrentConditions {
		let model = CurrentConditions()
		
		if let icon = json["WeatherIcon"] as? Int {
			let iconStr = icon < 10 ? "0\(icon)" : "\(icon)"
			model.iconPath = "https://developer.accuweather.com/sites/default/files/\(iconStr)-s.png"
		}
		
		if let temp = json["Temperature"] as? [String: AnyObject], let metric = temp["Metric"] as? [String: AnyObject] {
			model.temperature = metric["Value"] as? Double ?? 0.0
		}
		
		model.weatherText = json["WeatherText"] as? String
		
		return model
	}
}
