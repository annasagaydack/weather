//
//  UIConfigurator.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 11/29/18.
//  Copyright © 2018 Anna Sahaidak. All rights reserved.
//

import UIKit

struct BaseColors {
	static let baseDark = UIColor.color(from: 0x6F7179)
}
