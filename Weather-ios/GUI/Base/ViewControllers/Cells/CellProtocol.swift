//
//  CellProtocol.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

protocol CellProtocol: class {
	
	func setup(with model: AnyObject?)
}
