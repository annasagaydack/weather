//
//  BaseCollectionViewCell.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell, CellProtocol {

	override init(frame: CGRect) {
		super.init(frame: frame)
		
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		setup()
	}
	
	func setup() {
		backgroundColor = UIColor.clear
	}
	
	
	//MARK: - CellProtocol
	
	func setup(with model: AnyObject?) {
		
	}
}
