//
//  BaseViewController.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 11/29/18.
//  Copyright © 2018 Anna Sahaidak. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
	
	var presenter: BasePresenter?
	
	
	// MARK:- ViewController lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		presenter = createPresenter()
		presenter?.viewDidLoad()
		
		view.backgroundColor = BaseColors.baseDark
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		presenter?.viewWillAppear()
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	
	//MARK: - presenter
	
	func createPresenter() -> BasePresenter? {
		return nil
	}
}
