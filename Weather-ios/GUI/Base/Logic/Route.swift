//
//  Route.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

class Route {

	let window: UIWindow
	
	init(window aWindow: UIWindow) {
		window = aWindow
	}
	
	func switchTo(_ vc: UIViewController) {
		
		let snapShot = window.snapshotView(afterScreenUpdates: false)
		
		if let snapShot = snapShot {
			window.addSubview(snapShot)
		}
		
		dismiss(vc) {
			self.window.rootViewController = vc
			
			if let snapShot = snapShot {
				self.window.bringSubviewToFront(snapShot)
				UIView.animate(withDuration: 0.3, animations: {
					snapShot.layer.opacity = 0
				}, completion: { _ in
					DispatchQueue.main.async {
						snapShot.removeFromSuperview()
					}
				})
			}
		}
	}
	
	func dismiss(_ vc: UIViewController, completion: @escaping (() -> Void)) {
		if vc.presentedViewController != nil {
			self.dismiss(vc, completion: {
				vc.dismiss(animated: false, completion: {
					completion()
				})
			})
		} else {
			completion()
		}
	}
}
