//
//  BasePresenter.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

class BasePresenter {
	
	func viewDidLoad() {
		
	}
	
	func viewWillAppear() {
		reloadData()
	}

	func reloadData() {
		
	}
}
