//
//  ForecastCell.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit

class ForecastCell: BaseCollectionViewCell {
	
	@IBOutlet weak private var ivConditions: UIImageView!
	@IBOutlet weak private var lbMinTemperature: UILabel!
	@IBOutlet weak private var lbMaxTemperature: UILabel!
	@IBOutlet weak private var lbPrecipitation: UILabel!
	@IBOutlet weak private var lbWindSpeed: UILabel!
	@IBOutlet weak private var lbDay: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		
		lbMinTemperature.adjustsFontSizeToFitWidth = true
		lbMaxTemperature.adjustsFontSizeToFitWidth = true
		
		clear()
    }
	
	override func prepareForReuse() {
		super.prepareForReuse()
		
		ivConditions.cancelDownloading()
		clear()
	}

	override func setup(with model: AnyObject?) {
		super.setup(with: model)
		
		if let forecast = model as? DailyForecast {
			if let path = forecast.iconPath, let url = URL(string: path) {
				ivConditions.setImage(with: url)
			}
			lbMinTemperature.text = forecast.minTemperature == 0.0 ? "--" : "\(Int(forecast.minTemperature))°"
			lbMaxTemperature.text = forecast.maxTemperature == 0.0 ? "--" : "... \(Int(forecast.maxTemperature))°"
			lbPrecipitation.text = "Rainfall \(forecast.precipitation)%"
			lbWindSpeed.text = "Wind \(forecast.windSpeed) km/h"
			lbDay.text = forecast.day
		}
	}
	
	private func clear() {
		ivConditions.image = nil
		lbMaxTemperature.text = "--"
		lbMinTemperature.text = "--"
		lbPrecipitation.text = "0 %"
		lbWindSpeed.text = "0 km/h"
		lbDay.text = nil
	}
}
