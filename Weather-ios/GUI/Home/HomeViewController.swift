//
//  HomeViewController.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit
import ScrollableGraphView

protocol DataReloadProtocol {
	func reloadData(_ data: [DailyForecast])
	func updateCurrentConditions(_ model: CurrentConditions)
}

final class HomeViewController: BaseViewController {
	
	@IBOutlet weak private var lbWeather: UILabel!
	@IBOutlet weak private var lbDay: UILabel!
	@IBOutlet weak private var lbTemperature: UILabel!
	@IBOutlet weak private var ivIcon: UIImageView!
	@IBOutlet weak private var collectionView: UICollectionView!
	@IBOutlet weak private var vGraph: UIView!
	private var graphView: ScrollableGraphView!
	private let pointSpacing: CGFloat = 75
	
	
	private var custPresenter: HomePresenter? {
		return presenter as? HomePresenter
	}
	
	private var dataSource: [DailyForecast] = []
	

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.register(UINib(nibName: String(describing: ForecastCell.self), bundle: nil), forCellWithReuseIdentifier: "ForecastCellId")
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
		vGraph.addGestureRecognizer(tap)
		
		lbWeather.adjustsFontSizeToFitWidth = true
		lbDay.text = "\(Date().shortDate()) today"

		configureGraph()
    }
	
	override func createPresenter() -> BasePresenter? {
		return HomePresenter(view: self)
	}
	
	
	//MARK: - Actions
	
	@objc func handleTap(sender: UITapGestureRecognizer? = nil) {
		if let location = sender?.location(in: vGraph).x {
			let index = Int(location / pointSpacing)
			if index >= 0 && index <= dataSource.count - 1 {
				collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
			}
		}
	}
	
	private func configureGraph() {
		graphView = ScrollableGraphView(frame: vGraph.bounds, dataSource: self)
		graphView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		vGraph.addSubview(graphView)
		
		// Setup the line plot.
		let linePlot = LinePlot(identifier: "darkLine")
		
		linePlot.lineWidth = 1
		linePlot.lineColor = UIColor.color(from: 0x777777)
		linePlot.lineStyle = ScrollableGraphViewLineStyle.smooth
		
		linePlot.shouldFill = true
		linePlot.fillType = ScrollableGraphViewFillType.gradient
		linePlot.fillGradientType = ScrollableGraphViewGradientType.linear
		linePlot.fillGradientStartColor = UIColor.color(from: 0x555555)
		linePlot.fillGradientEndColor = UIColor.color(from: 0x444444)
		
		linePlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
		
		let dotPlot = DotPlot(identifier: "darkLineDot") // Add dots as well.
		dotPlot.dataPointSize = 2
		dotPlot.dataPointFillColor = UIColor.white
		
		dotPlot.adaptAnimationType = ScrollableGraphViewAnimationType.elastic
		
		// Setup the reference lines.
		let referenceLines = ReferenceLines()
		
		referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 8)
		referenceLines.referenceLineColor = UIColor.white.withAlphaComponent(0.2)
		referenceLines.referenceLineLabelColor = UIColor.white
		
		referenceLines.positionType = .absolute
		// Reference lines will be shown at these values on the y-axis.
		referenceLines.absolutePositions = [-2, 0, 2]
		referenceLines.includeMinMax = true
		referenceLines.dataPointLabelColor = UIColor.white.withAlphaComponent(0.5)
		
		// Setup the graph
		graphView.backgroundFillColor = UIColor.color(from: 0x333333)
		graphView.dataPointSpacing = pointSpacing
		
		graphView.shouldAnimateOnStartup = true
		graphView.shouldAdaptRange = true
		
		// Add everything to the graph.
		graphView.addReferenceLines(referenceLines: referenceLines)
		graphView.addPlot(plot: linePlot)
		graphView.addPlot(plot: dotPlot)
	}
}


extension HomeViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return dataSource.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCellId", for: indexPath) as! ForecastCell
		cell.setup(with: dataSource[indexPath.item])
		
		return cell
	}
}


extension HomeViewController: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 250, height: 100)
	}
}

extension HomeViewController: DataReloadProtocol {
	
	func updateCurrentConditions(_ model: CurrentConditions) {
		if let path = model.iconPath, let url = URL(string: path) {
			ivIcon.setImage(with: url)
		}
		
		lbTemperature.text = model.temperature == 0.0 ? "" : "\(Int(model.temperature))°"
		lbWeather.text = model.weatherText
	}
	
	func reloadData(_ data: [DailyForecast]) {
		dataSource = data
		collectionView.reloadData()
		
		graphView.removeFromSuperview()
		configureGraph()
		graphView.reload()
	}
}


extension HomeViewController: ScrollableGraphViewDataSource {
	func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
		return dataSource[pointIndex].maxTemperature
	}
	
	func label(atIndex pointIndex: Int) -> String {
		return dataSource[pointIndex].day
	}
	
	func numberOfPoints() -> Int {
		return dataSource.count
	}
}
