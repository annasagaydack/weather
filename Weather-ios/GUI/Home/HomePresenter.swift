//
//  HomePresenter.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit
import RealmSwift

class HomePresenter: BasePresenter {
	
	private var models: [DailyForecast] {		
		return DailyForecast.allObjects()
	}
	
	let view: DataReloadProtocol?
	
	init(view: DataReloadProtocol) {
		self.view = view
	}
	
	override func reloadData() {
		super.reloadData()
		
		if BaseNetworkService.isInternetReachible {
			loadCurrentConditions()
			loadForecast()
		} else {
			view?.reloadData(models)
		}
	}
	
	private func loadCurrentConditions() {
		ForecastNetworkService.getCurrentConditions(successHandler: { [weak self] (response) in
			if let data = response {
				let model: CurrentConditions = CurrentConditions.parseJson(data as [String: AnyObject])
				
				self?.view?.updateCurrentConditions(model)
			}
		})
	}
	
	private func loadForecast() {
		ForecastNetworkService.getForecast(successHandler: { [weak self] (response) in
			if let data = response?["DailyForecasts"] as? [[String: AnyObject]] {
				let forecasts: [DailyForecast] = data.map { DailyForecast.parseJson($0) }
				
				let realm = try! Realm()
				try! realm.write {
					realm.deleteAll()
					realm.add(forecasts)
				}
				
				self?.view?.reloadData(forecasts)
			}
		})
	}
}
