//
//  ForecastNetworkService.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import UIKit
import Alamofire

class ForecastNetworkService: BaseNetworkService {

	static func getForecast(successHandler: @escaping NetworkSuccessHandler) {
		
		let params: [String : Any] = [
			"apikey": AppConfig.apiKey,
			"language": "en-us",
			"details": "true",
			"metric": "true"
		]
		
		ForecastNetworkService.sendRequest(.get, request: "forecasts/v1/daily/5day/\(AppConfig.cityCode)", params: params, successHandler: successHandler)
	}
	
	static func getCurrentConditions(successHandler: @escaping NetworkSuccessHandler) {
		
		let params: [String : Any] = [
			"apikey": AppConfig.apiKey,
			"language": "en-us",
			"details": "false",
		]
		
		ForecastNetworkService.sendRequest(.get, request: "currentconditions/v1/\(AppConfig.cityCode)", params: params, successHandler: successHandler)
	}
}
