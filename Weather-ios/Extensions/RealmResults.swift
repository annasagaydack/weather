//
//  RealmResults.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
	
	func array() -> [Element] {
		var objects: [Element] = []
		for object in self {
			objects.append(object as Element)
		}
		
		return objects
	}
}
