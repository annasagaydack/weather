//
//  UIImage+Help.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 11/29/18.
//  Copyright © 2018 Anna Sahaidak. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
	
	func setImage(with url: URL?, useActivity: Bool = false) {
		var kf = self.kf
		
		kf.indicatorType = useActivity ? .activity : .none
		kf.setImage(with: url)
	}
	
	func cancelDownloading() {
		
		self.kf.cancelDownloadTask()
	}
}
