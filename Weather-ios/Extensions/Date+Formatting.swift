//
//  Date+Formatting.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 3/1/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import Foundation

private let baseDateFormat = "E. dd"
private let shortDateFormat = "EEEE"

extension Date {
	
	func shortDate() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = shortDateFormat
		return dateFormatter.string(from: self)
	}
	
	func baseConvertToString() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = baseDateFormat
		return dateFormatter.string(from: self)
	}
}
