//
//  UIColor+RGB.swift
//  Weather-ios
//
//  Created by Anna Sahaidack on 11/29/18.
//  Copyright © 2018 Anna Sahaidak. All rights reserved.
//

import UIKit

extension UIColor {
	class func color(from rgbValue: UInt) -> UIColor {
		return UIColor(
			red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
			alpha: CGFloat(1.0)
		)
	}
}
