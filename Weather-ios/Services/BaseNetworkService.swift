//
//  BaseNetworkService.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 2/22/19.
//  Copyright © 2019 Anna Sahaidak. All rights reserved.
//

import Foundation
import Alamofire


typealias NetworkSuccessHandler = ([String: Any]?) -> ()

class BaseNetworkService {
	
	static var isInternetReachible: Bool {
		return NetworkReachabilityManager()?.isReachable == true
	}
	
	static let imageCache = NSCache<NSString, UIImage>()

	static let baseUrl = "http://dataservice.accuweather.com/"
	
	static let configuration = URLSessionConfiguration.default
	static let alamofireManager = Alamofire.SessionManager(configuration: BaseNetworkService.configuration)
	
	class func sendRequest(_ method: HTTPMethod, request: String, params: [String: Any], successHandler: @escaping NetworkSuccessHandler) {
		
		var encoding: ParameterEncoding = JSONEncoding.default

		if method == .get {
			encoding = URLEncoding.default
		}
		
		let request = alamofireManager.request("\(BaseNetworkService.baseUrl)/\(request)", method: method, parameters: params, encoding: encoding, headers: nil).responseJSON { (response) in
			if response.result.isSuccess {
				if let data = response.result.value as? [String: AnyObject] {
					successHandler(data)
				} else if let data = response.result.value as? [[String: AnyObject]] {
					successHandler(data.first)
				}
			}
		}
		
		#if DEBUG
		print(request)
		#endif
	}
}
