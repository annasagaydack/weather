//
//  AppManager.swift
//  Weather-ios
//
//  Created by Anna Sahaidak on 11/29/18.
//  Copyright © 2018 Anna Sahaidak. All rights reserved.
//

import UIKit

final class AppManager {

	static let shared = AppManager()
	
	private init() {}
	
	let route = Route(window: ((UIApplication.shared.delegate?.window)!)!)
	
	func updateRootViewController() {
		route.switchToInitial()
	}
}

extension Route {
	
	func switchToInitial() {
		let vc = HomeViewController()
		switchTo(vc)
	}
}
